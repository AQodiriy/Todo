//
//  Unregistered.swift
//  To do
//
//  Created by Ahmadxon Qodirov on 24/09/23.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class Unregistered: BaseViewController {
    
    let databaseRef = Database.database().reference()
    
    let profileImg = UIImageView()
    let profileView = UIView()
    let profileText = UILabel()
    
    let email = UILabel()
    let penView = UIButton()
    let profileImg2 = UIImageView()
    
    let register = UIButton()
    let regText = UILabel()
    let regIcon = UIImageView()
    
    let signin = UIButton()
    let signinText = UILabel()
    let signinIcon = UIImageView()
    
    // registered
    let share = UIButton()
    let shareIcon = UIImageView()
    let shareText = UILabel()
    
    let logout = UIButton()
    let logoutIcon = UIImageView()
    let logoutText = UILabel()
    
    
    let seconCV = ProfileImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let selectedImage = self.defaults.object(forKey: "savedImage") as? String {
            self.profileImg2.image = UIImage(named: selectedImage)
            
            
            isRegistered()
        }
       
    }
    
    
    
    func setView(){
        
        profileView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(profileView)
        profileView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        profileView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 32).isActive = true
        profileView.heightAnchor.constraint(equalToConstant: 102).isActive = true
        profileView.widthAnchor.constraint(equalToConstant: 102).isActive = true
       
        profileView.layer.cornerRadius = 50
        profileView.layer.borderColor = UIColor.gray.cgColor
        profileView.layer.borderWidth = 2
        profileView.clipsToBounds = true
        
        profileImg.translatesAutoresizingMaskIntoConstraints = false
        profileView.addSubview(profileImg)
        profileImg.centerXAnchor.constraint(equalTo: profileView.centerXAnchor).isActive = true
        profileImg.centerYAnchor.constraint(equalTo: profileView.centerYAnchor).isActive = true
        profileImg.heightAnchor.constraint(equalToConstant: 72).isActive = true
        profileImg.widthAnchor.constraint(equalToConstant: 58).isActive = true
        profileImg.image = UIImage(named: "man")
        
        profileText.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(profileText)
        profileText.topAnchor.constraint(equalTo: profileView.bottomAnchor, constant: 18).isActive = true
        profileText.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        profileText.widthAnchor.constraint(equalToConstant: 280).isActive = true
        profileText.text = "Ko’proq imkoniyatlarga ega bo’lish uchun iltimos ro’yxatdan o’ting!"
        profileText.numberOfLines = 5
        
        register.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(register)
        register.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        register.topAnchor.constraint(equalTo: profileText.bottomAnchor, constant: 17).isActive = true
        register.heightAnchor.constraint(equalToConstant: 56).isActive = true
        register.widthAnchor.constraint(equalToConstant: 333).isActive = true
        register.layer.borderWidth = 1
        register.layer.cornerRadius = 12
        
        regIcon.translatesAutoresizingMaskIntoConstraints = false
        register.addSubview(regIcon)
        regIcon.centerYAnchor.constraint(equalTo: register.centerYAnchor).isActive = true
        regIcon.leftAnchor.constraint(equalTo: register.leftAnchor,constant: 20).isActive = true
        //regIcon.rightAnchor.constraint(equalTo: register.rightAnchor,constant: -290).isActive = true
        regIcon.heightAnchor.constraint(equalToConstant: 18).isActive = true
        regIcon.widthAnchor.constraint(equalToConstant: 18).isActive = true
        regIcon.image = UIImage(named: "man2")
        
        regText.translatesAutoresizingMaskIntoConstraints = false
        register.addSubview(regText)
        regText.centerYAnchor.constraint(equalTo: register.centerYAnchor).isActive = true
        regText.centerXAnchor.constraint(equalTo: register.centerXAnchor).isActive = true
        regText.text = "Ro’yxatdan o’tish"
        
        signin.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(signin)
        signin.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        signin.topAnchor.constraint(equalTo: register.bottomAnchor, constant: 8).isActive = true
        signin.heightAnchor.constraint(equalToConstant: 56).isActive = true
        signin.widthAnchor.constraint(equalToConstant: 333).isActive = true
        signin.layer.borderWidth = 1
        signin.layer.cornerRadius = 12
        
        signinIcon.translatesAutoresizingMaskIntoConstraints = false
        signin.addSubview(signinIcon)
        signinIcon.centerYAnchor.constraint(equalTo: signin.centerYAnchor).isActive = true
        signinIcon.leftAnchor.constraint(equalTo: signin.leftAnchor,constant: 20).isActive = true
        //regIcon.rightAnchor.constraint(equalTo: register.rightAnchor,constant: -290).isActive = true
        signinIcon.heightAnchor.constraint(equalToConstant: 24).isActive = true
        signinIcon.widthAnchor.constraint(equalToConstant: 24).isActive = true
        signinIcon.image = UIImage(named: "login")
        
        signinText.translatesAutoresizingMaskIntoConstraints = false
        signin.addSubview(signinText)
        signinText.centerYAnchor.constraint(equalTo: signin.centerYAnchor).isActive = true
        signinText.centerXAnchor.constraint(equalTo: signin.centerXAnchor).isActive = true
        signinText.text = "Kirish"
        
        
        
        // registered
        
        profileImg2.translatesAutoresizingMaskIntoConstraints = false
        profileView.addSubview(profileImg2)
        profileImg2.centerXAnchor.constraint(equalTo: profileView.centerXAnchor).isActive = true
        profileImg2.centerYAnchor.constraint(equalTo: profileView.centerYAnchor).isActive = true
        profileImg2.image = UIImage(named: "man")
        profileImg2.heightAnchor.constraint(equalToConstant: 112).isActive = true
        profileImg2.widthAnchor.constraint(equalToConstant: 112).isActive = true
    
      
       
        
        
        penView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(penView)
        penView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 35).isActive = true
        penView.leftAnchor.constraint(equalTo: profileView.leftAnchor,constant: 79).isActive = true
        penView.heightAnchor.constraint(equalToConstant: 29).isActive = true
        penView.widthAnchor.constraint(equalToConstant: 29).isActive = true
        penView.setImage(UIImage(named: "pen"), for: .normal)
        penView.addTarget(self, action: #selector(profileImage), for: .touchUpInside)
        
        email.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(email)
        email.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        email.topAnchor.constraint(equalTo: profileView.bottomAnchor, constant: 24).isActive = true
        email.text = "JonDoe@gmail.com"
        
        
        share.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(share)
        share.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        share.topAnchor.constraint(equalTo: email.bottomAnchor, constant: 26).isActive = true
        share.heightAnchor.constraint(equalToConstant: 56).isActive = true
        share.widthAnchor.constraint(equalToConstant: 333).isActive = true
        share.layer.borderWidth = 1
        share.layer.cornerRadius = 12
        share.addTarget(self, action: #selector(shareTap), for: .touchUpInside)
        
        shareIcon.translatesAutoresizingMaskIntoConstraints = false
        share.addSubview(shareIcon)
        shareIcon.centerYAnchor.constraint(equalTo: share.centerYAnchor).isActive = true
        shareIcon.leftAnchor.constraint(equalTo: share.leftAnchor,constant: 20).isActive = true
        shareIcon.heightAnchor.constraint(equalToConstant: 18).isActive = true
        shareIcon.widthAnchor.constraint(equalToConstant: 18).isActive = true
        shareIcon.image = UIImage(named: "share")
        
        shareText.translatesAutoresizingMaskIntoConstraints = false
        share.addSubview(shareText)
        shareText.centerYAnchor.constraint(equalTo: share.centerYAnchor).isActive = true
        shareText.centerXAnchor.constraint(equalTo: share.centerXAnchor).isActive = true
        shareText.text = "Ilovani ulashish"
        
        logout.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(logout)
        logout.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        logout.topAnchor.constraint(equalTo: share.bottomAnchor, constant: 8).isActive = true
        logout.heightAnchor.constraint(equalToConstant: 56).isActive = true
        logout.widthAnchor.constraint(equalToConstant: 333).isActive = true
        logout.layer.borderWidth = 1
        logout.layer.cornerRadius = 12
        logout.addTarget(self, action: #selector(logoutTap), for: .touchUpInside)
        
        logoutIcon.translatesAutoresizingMaskIntoConstraints = false
        logout.addSubview(logoutIcon)
        logoutIcon.centerYAnchor.constraint(equalTo: signin.centerYAnchor).isActive = true
        logoutIcon.leftAnchor.constraint(equalTo: signin.leftAnchor,constant: 20).isActive = true
        logoutIcon.heightAnchor.constraint(equalToConstant: 24).isActive = true
        logoutIcon.widthAnchor.constraint(equalToConstant: 24).isActive = true
        logoutIcon.image = UIImage(named: "logout")
        
        logoutText.translatesAutoresizingMaskIntoConstraints = false
        logout.addSubview(logoutText)
        logoutText.centerYAnchor.constraint(equalTo: signin.centerYAnchor).isActive = true
        logoutText.centerXAnchor.constraint(equalTo: signin.centerXAnchor).isActive = true
        logoutText.text = "Profildan chiqish"
        
        
       
       
        
        
    }
    func isRegistered(){
        let usersRef = databaseRef.child("users")
        
        if let userId = Auth.auth().currentUser?.uid {
            // registered
            profileImg2.isHidden = false
            email.isHidden = false
            penView.isHidden = false
            share.isHidden = false
            shareIcon.isHidden = false
            shareText.isHidden = false
            logout.isHidden = false
            logoutIcon.isHidden = false
            logoutText.isHidden = false
            
            // unregistered
            profileImg.isHidden = true
            profileText.isHidden = true
            penView.isHidden = true
            register.isHidden = true
            regIcon.isHidden = true
            regText.isHidden = true
            signin.isHidden = true
            signinIcon.isHidden = true
            signinText.isHidden = true
            
            usersRef.child(userId).observeSingleEvent(of: .value){ (snapshot) in
                let userEmail = snapshot.childSnapshot(forPath: "email").value as? String
                self.email.text = userEmail
            }
            
        } else {
            // not registered
            profileImg.isHidden = false
            profileText.isHidden = false
            penView.isHidden = false
            register.isHidden = false
            regIcon.isHidden = false
            regText.isHidden = false
            signin.isHidden = false
            signinIcon.isHidden = false
            signinText.isHidden = false
            
            // registered
            profileImg2.isHidden = true
            email.isHidden = true
            penView.isHidden = true
            share.isHidden = true
            shareIcon.isHidden = true
            shareText.isHidden = true
            logout.isHidden = true
            logoutIcon.isHidden = true
            logoutText.isHidden = true
        }
    }
    
    
    
    
    @objc func profileImage(){
        navigationController?.pushViewController(ProfileImage(), animated: true)
        
    }
    
    @objc func logoutTap(){
        let exit = ExitProfile()
        exit.modalPresentationStyle = .formSheet
        present(exit, animated: true, completion: nil)
        if let sheet = exit.sheetPresentationController {
            sheet.detents = [
                .custom { _ in
                    return 180
                }
            ]
        }
    }
    
    @objc func shareTap(){
        let items: [Any] = ["This is additional text",URL(string: "https://www.apple.com")!] 
        let activity = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(activity, animated: true)
        print("working share button")
    }
}
