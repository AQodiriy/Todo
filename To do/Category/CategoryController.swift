//
//  CategoryController.swift
//  To do
//
//  Created by Ahmadxon Qodirov on 22/07/23.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth

typealias CatDataSource = UITableViewDiffableDataSource<CatSection, CategoryData>
typealias CatSnapshot = NSDiffableDataSourceSnapshot<CatSection,CategoryData>

class CategoryController: BaseViewController {
    
    var catTitle = UILabel()
    let tableView = UITableView()
    let addButton = UIButton()
    let color = ColorPick()
    
    var catList: [CategoryData] = []
    var selectedCategory: CategoryData?
    let ref = Database.database().reference()
    
     lazy var dataSource = makeDataSource()
    var snapshot = CatSnapshot()
   
    override func viewWillAppear(_ animated: Bool) {
       // super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(red: 0.97, green: 0.97, blue: 0.97, alpha: 1)
        setSearch()
        setTable()
        setButton()
        fireBaseGet()
        navigationController?.navigationBar.topItem?.title = "Category"
    }
    
    func makeDataSource() -> CatDataSource {
        let dataSource = CatDataSource(tableView: tableView, cellProvider: {  (tableView, indexPath, category) -> UITableViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? CategoryCell
            
            cell?.category = category
            cell?.backgroundColor = .white
            return cell
            
        })
        return dataSource
    }
    
    func applySnapshot(animatingDifferences: Bool = false) {
        var snapshot = CatSnapshot()
        if !snapshot.sectionIdentifiers.contains(.main) {
            snapshot.appendSections([.main])
        }
        
        //  snapshot.appendSections([.main])
        
        snapshot.appendItems(catList, toSection: .main)
        
        dataSource.apply(snapshot, animatingDifferences: true)
        print("CategoryList111 \(catList.count)")
       
    }
    
    
    
    
    func setSearch(){
        view.addSubview(catTitle)
     //   catTitle.frame = CGRect(x: 10, y: 51, width: 115, height: 25)
//        catTitle.text = "Category"
//        catTitle.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
    
        
        
    }
    
    func setTable(){
        tableView.delegate = self
        tableView.register(CategoryCell.self, forCellReuseIdentifier: "cell")
        
        
        
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.topAnchor,constant: 80).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor,constant: 15).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor,constant: -15).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor(red: 0.97, green: 0.97, blue: 0.97, alpha: 1)
       
    }
    
    
    func setButton(){
        addButton.translatesAutoresizingMaskIntoConstraints = false
        addButton.setImage(UIImage(named: "plus"), for: .normal)
        addButton.backgroundColor = .systemBlue
        addButton.layer.cornerRadius = 25
        
        view.addSubview(addButton)
        addButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -15).isActive = true
        addButton.rightAnchor.constraint(equalTo: view.rightAnchor,constant: -20).isActive = true
        addButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        addButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        addButton.addTarget(self, action: #selector(addButtonTapped), for: .touchUpInside)
    }
    
    @objc func addButtonTapped(){
        print("add button tapped in category")
        let bottomShCV = CategoryBottomSheet()
            bottomShCV.delegate = self
      //  bottomShCV.textField.text = ""
       // categoryBottomSheet.color.selectedColors(UIColor.black)
        present((bottomShCV), animated: true)
        modalPresentationStyle = .formSheet
        
        if let sheet = bottomShCV.sheetPresentationController {
            
            sheet.detents = [
                .custom { _ in
                    return 657
                }
            ]
        }
        
    }
    // MARK: - Get data from firebase
     func fireBaseGet(){
         self.catList.removeAll()
         
         guard let userId = Auth.auth().currentUser?.uid else { return }
         let userRef = self.ref.child("users").child(userId)
         userRef.child("category").observe( .childAdded, with: { snapshot in
                    
                    if let category = snapshot.value as? [String: Any] {
                    let name = category["name"] as? String
                    let color = category["color"] as? String
                    let id = category["id"] as? String
                        
                    // let colorId = color["id"] as? String
                    //  let colorName = color?["color"] as? String
                    //  print("Color name: -> \(colorName) ;; Color id: \(colorId)")
                    
                    let colorModel = ColorModel(color: color ?? "000000")
                    let categoryData = CategoryData(name: name ?? "no name", color: colorModel, id: id ?? "nil")
                    self.catList.append(categoryData)
                        
                       
            }
             self.applySnapshot()
             
        })
    }
}
// selectedRow
extension CategoryController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let catData = dataSource.itemIdentifier(for: indexPath) else {return}
        let categoryBottomSheet = CategoryBottomSheet(catData: catData)
        categoryBottomSheet.delegate = self
            present((categoryBottomSheet), animated: true)
            modalPresentationStyle = .formSheet
    }
    // delete
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let deleteAction = UIContextualAction(style: .destructive, title: nil) { [weak self] (action, view, completionHandler) in
                    guard let self = self else { return }
                    
                    // Get the item to delete
                    guard let item = self.dataSource.itemIdentifier(for: indexPath) else {
                        completionHandler(false)
                        return
                    }
                    // Update data source
                    var snapshot = self.dataSource.snapshot()
                    
                    if let itemIndex = self.catList.firstIndex(where: { $0.id == item.id }) {
                        self.catList.remove(at: itemIndex)
                        snapshot.deleteItems([item])
                        let key = item.id
                        let deleteItem = self.ref.child("category").child(key!)
                        deleteItem.removeValue()
                    }
                    // firebase delete
                    self.dataSource.apply(snapshot)
                    completionHandler(true)
                }
                deleteAction.image = UIImage(named: "delete")
                deleteAction.backgroundColor = UIColor(red: 0.97, green: 0.97, blue: 0.97, alpha: 1)
                let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
                configuration.performsFirstActionWithFullSwipe = true
                return configuration
    }
}
// update
extension CategoryController: CatUpdate {
    func catUpdate(_ controller: CategoryBottomSheet, didModifyItem category: CategoryData) {
        if let index = catList.firstIndex(where: {$0.id == category.id}) {
            catList[index] = category
            print("catList category: \(catList[index])")
            var snapshot = dataSource.snapshot()
            snapshot.reloadItems(catList)
            dataSource.apply(snapshot)
        }
    }
}


extension CategoryController: UISearchBarDelegate {
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.setShowsCancelButton(true, animated: true)
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
    }
}
