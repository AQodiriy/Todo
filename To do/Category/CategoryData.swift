//
//  CategoryData.swift
//  To do
//
//  Created by Ahmadxon Qodirov on 08/08/23.
//

import UIKit


enum CatSection {
    case main
}

class CategoryData: Hashable, Identifiable {
    
    
    var name: String?
    var color: ColorModel?
    var id: String?
    
    init(name: String, color: ColorModel, id: String) {
        self.name = name
        self.color = color
        self.id = id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    static func == (lhs: CategoryData, rhs: CategoryData) -> Bool {
        lhs.id = rhs.id
        return true
    }
}



//extension CategoryData {
//    static let allCategory = [
//        CategoryData(title: "Homework", id: UUID()),
//        CategoryData(title: "Workout",id: UUID()),
//        CategoryData(title: "Grammar",id: UUID()),
//        CategoryData(title: "Work",id: UUID()),
//    ]
//}
