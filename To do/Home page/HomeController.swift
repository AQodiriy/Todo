//
//  HomeController.swift
//  To do
//
//  Created by Ahmadxon Qodirov on 22/07/23.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth


typealias DataSource = UITableViewDiffableDataSource<Section, ToDo>
typealias Snapshot = NSDiffableDataSourceSnapshot<Section, ToDo>

class HomeController: BaseViewController {
    
    private lazy var dataSource = makeDataSource()
    var itemList: [ToDo] = []
    var categoryList: [CategoryData] = []
    let ref = Database.database().reference()
    var categoryModel: CategoryData?
    
    let tableView = UITableView()
    let headerView = HeaderView()
    let addButton = UIButton()
    var snapshot = Snapshot()
    
    var selectedCategory: CategoryData?
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        self.navigationController?.setNavigationBarHidden(true, animated: animated)
//    }
    
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setViews()
       firebase()
       
        
    }
    
    func makeDataSource() -> DataSource {
        
        let dataSource = DataSource(tableView: tableView, cellProvider: {(tableView, indexPath, item) ->
            UITableViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: "notes", for: indexPath) as? TodoCell
            
            cell?.item = item
            
//            headerView.selectCategory = { (category, category2) in
//                let filteredItems = itemList.sorted {$0.category?.id == category.category?.id  }
//
//            }
            
//            self.headerView.selectCategory = { category in
//                let filteredItems = self.itemList.sorted { todo1, todo2 in
//                    return todo1.category?.id == category.category?.id
//                }
//                cell?.item = filteredItems[indexPath.item]
//
//            }
            cell?.selectionStyle = .none
            
            return cell
            
        })
        return dataSource
    }
    
    func applySnapshot(animatingDifferences: Bool = false){
        
        var snapshot = Snapshot()
        if !snapshot.sectionIdentifiers.contains(.main) {
            snapshot.appendSections([.main])
            
        }
        
        
     //   let filteredItems = itemList.filter { $0.category?.id == selectedCategory?.id }
     //   snapshot.appendItems(filteredItems, toSection: .main)
        
        self.headerView.selectCategory = { category in
            let filteredItems = self.itemList.filter { $0.category?.id == category.category?.id}
                
            snapshot.appendItems(filteredItems, toSection: .main)
            self.headerView.viewDidAppear()
            self.applySnapshot()
        
            self.dataSource.apply(snapshot)
            
            
        }
    }
    
    
    func setViews(){
        view.addSubview(tableView)
        
        tableView.tableHeaderView = headerView
        tableView.delegate = self
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(TodoCell.self, forCellReuseIdentifier: "notes")
        
        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,constant: 0).isActive = true
        tableView.backgroundColor = UIColor(red: 0.97, green: 0.97, blue: 0.97, alpha: 1)
        tableView.separatorStyle = .none

        headerView.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: 375, height: 155))
        headerView.backgroundColor = UIColor(red: 0.97, green: 0.97, blue: 0.97, alpha: 1)
        
        
        
        // button
        addButton.translatesAutoresizingMaskIntoConstraints = false
        addButton.setImage(UIImage(named: "plus"), for: .normal)
        addButton.backgroundColor = .systemBlue
        addButton.layer.cornerRadius = 25
        
        view.addSubview(addButton)
       // addButton.topAnchor.constraint(equalTo: view.topAnchor,constant: 618).isActive = true
        addButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -15).isActive = true
       // addButton.leftAnchor.constraint(equalTo: view.leftAnchor,constant: 305).isActive = true
        addButton.rightAnchor.constraint(equalTo: view.rightAnchor,constant: -20).isActive = true
        addButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        addButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        addButton.addTarget(self, action: #selector(addButtonTapped), for: .touchUpInside)
        
    }
    
    @objc func addButtonTapped() {
        let noteVC = NoteViewController()
       // noteVC.delegate = self
        navigationController?.pushViewController(noteVC, animated: true)
      
    }
    
    // get
    func firebase(){
        // read
        self.itemList.removeAll()
        // category
        
        guard let userId = Auth.auth().currentUser?.uid else { return }
        let userRef = self.ref.child("users").child(userId)
        
        
        userRef.child("category").observe(.childAdded, with: { snapshot in
            if let category = snapshot.value as? [String: Any] {
                
                let catName = category["name"] as? String
                let catColor = category["color"] as? String
                let catId = category["id"] as? String
                
                let colorModel = ColorModel(color: catColor ?? "000000")
                let categoryData = CategoryData(name: catName ?? "no name", color: colorModel, id: catId ?? "nil")
                self.categoryList.append(categoryData)
               
            }
            self.applySnapshot()
            print("category: \(self.categoryList)")
           
        })
        // todo
        userRef.child("todo").observe(.childAdded, with: {snapshot in
            if let todo = snapshot.value as? [String: Any] {
               
                let id = todo["id"] as? String
                let title = todo["title"] as? String
                let body = todo["body"] as? String
                let status = todo["status"] as? String
                let categoryId = todo["categoryId"] as? String
               
                for cell in self.categoryList {
                    if cell.id == categoryId {
                        self.categoryModel = cell
                        let todoModel = ToDo(title: title!, body: body!, category: self.categoryModel!, status: status!, id: id ?? "todo id is nil")
                        
                        self.itemList.append(todoModel)
                        self.applySnapshot()
                      
                }
            }
        }
           
    })
        
        // update
        
        
        userRef.child("todo").observe(.childChanged, with: { [self] snapshot in
            if let todo = snapshot.value as? [String: Any] {
                let id = todo["id"] as? String
                let title = todo["title"] as? String
                let body = todo["body"] as? String
                let status = todo["status"] as? String
                let categoryId = todo["categoryId"] as? String
                
                for cell in self.categoryList {
                    if cell.id == categoryId {
                        let todoModel = ToDo(title: title!, body: body!, category: self.categoryModel!, status: status!, id: id!)
                        if let index = self.itemList.firstIndex(where: {$0.id == todoModel.id}){
                            self.itemList[index] = todoModel
                           
                        }
                       
                    }
                }
               
            }
            self.applySnapshot()
        })
        // delete
        userRef.child("todo").observe(.childRemoved, with: {snapshot in
            if let todo = snapshot.value as? [String: Any] {
                let id = todo["id"] as? String
                if let index = self.itemList.firstIndex(where: {$0.id == id }) {
                    self.itemList.remove(at: index)
                    self.applySnapshot()
                }
            }
        })
        
        
    }
    
    

}
extension HomeController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let item = dataSource.itemIdentifier(for: indexPath) else {return}
        
        let noteCV = NoteViewController(todo: item)
//        noteCV.item = item
        self.navigationController?.pushViewController(noteCV, animated: true)

    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let deleteAction = UIContextualAction(style: .destructive, title: nil) { [weak self] (action, view, completionHandler) in
                    guard let self = self else { return }
            
                    // Get the item to delete
                    guard let item = self.dataSource.itemIdentifier(for: indexPath) else {
                        completionHandler(false)
                        return
                    }
                    // Update data source
                    var snapshot = self.dataSource.snapshot()
                    
                    if let itemIndex = self.itemList.firstIndex(where: { $0.id == item.id }) {
                        self.itemList.remove(at: itemIndex)
                        snapshot.deleteItems([item])
                        let key = item.id
                        let deleteItem = self.ref.child("todo").child(key!)
                        deleteItem.removeValue()
                    }
                    // firebase delete
                    self.dataSource.apply(snapshot)
                    completionHandler(true)
                }
                deleteAction.image = UIImage(named: "delete")
                deleteAction.backgroundColor = UIColor(red: 0.97, green: 0.97, blue: 0.97, alpha: 1)
                let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
                configuration.performsFirstActionWithFullSwipe = true
                return configuration
    }
    
    
    
    
    
    
}

