//
//  TableData.swift
//  To do
//
//  Created by Ahmadxon Qodirov on 24/07/23.
//

import UIKit

enum Section {
    case main
}




class ToDo: Hashable, Identifiable {
    
    
    var id: String?
    var title: String?
    var body: String?
    var category: CategoryData?
    var status: String?
    var color: String?
    
    init(title: String, body: String, category: CategoryData, status: String, id: String) {
        self.id = id
        self.title = title
        self.body = body
        self.category = category
        self.status = status
        //self.color = color
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    
    static func == (lhs: ToDo, rhs: ToDo) -> Bool {
        lhs.id == rhs.id
    }
    
}


//extension Item {
//    static let allItems = [
//    Item(date: "2018/10/01", mainTitle: "Design for homepageDesign for homepageDesign for homepageDesign for homepageDesign for homepage", subTitle: "Here are tasks are written inside"),
//    Item(date: "2018/10/01", mainTitle: "Study english", subTitle: "new vocabulary list"),
//    Item(date: "2018/15/04", mainTitle: "Morning workout", subTitle: "stretch 5 minutes"),
//    
//    
//    
//    ]
//}
