//
//  ViewController.swift
//  To do
//
//  Created by Ahmadxon Qodirov on 22/07/23.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBar.backgroundColor = UIColor.white
        
        
        
       let home = HomeController()
        home.tabBarItem =  UITabBarItem(title: "Todo", image: UIImage(named: "home"), selectedImage: UIImage(named: "homeSelected"))
        let category = CategoryController()
        category.tabBarItem =  UITabBarItem(title: "Category", image: UIImage(named: "chart"), selectedImage: UIImage(named: "chartSelected"))
        let profile = Unregistered()
        profile.tabBarItem = UITabBarItem(title: "Profile", image: UIImage(named: "profile"), selectedImage: UIImage(named: "profileSelected"))
        
        let viewControllerList = [home, category, profile]
        viewControllers = viewControllerList.map {$0}
        
        tabBarController?.tabBar.delegate = self
        
        viewControllerList[1].navigationItem.title = "This is"
    }


}

extension TabBarController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

//extension UITabBar {
//    func barAppearance(){
//        self.barTintColor = .green
//       // self.tintColor = UIColor(red: 0, green: 0.39, blue: 1, alpha: 1)
//        UITabBar.appearance().barTintColor = UIColor.black
//    }
//}


