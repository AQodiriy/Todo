//
//  NoteViewController.swift
//  To do
//
//  Created by Ahmadxon Qodirov on 29/07/23.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

protocol NoteVCDelegate: AnyObject {
    func noteVController(_ controller: NoteViewController, didCreateNewItem: ToDo)
    func noteVConroller(_ controller: NoteViewController, didModifyItem: ToDo)
}


class NoteViewController: BaseViewController, UITextViewDelegate, UITextFieldDelegate {
    
    
  //  weak var delegate: NoteVCDelegate?
   
    var categoryList: [CategoryData] = []
    
    
        let categoryView = UIView()
        let categoryTitle = UILabel()
        let categoryIcon = UIImageView()
        let categoryButton = UIButton()
        
        var textView: UITextView!
        var textField: UITextField!
    
        var item: ToDo?
        var categoryData: CategoryData?
        let ref = Database.database().reference()
    
        let backIcon = UIImage(named: "back")
    
    
    init(todo: ToDo) {
        super.init(nibName: nil, bundle: nil)
        self.item = todo
    }
    init(){
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   
    override func viewWillAppear(_ animated: Bool) {
           // super.viewWillAppear(animated)
            self.navigationController?.isNavigationBarHidden = false
        }
   
        override func viewDidLoad() {
            super.viewDidLoad()
            view.backgroundColor = .white
            setCategory()
            setupTextField()
            setupTextView()
            textField.delegate = self
            textView.delegate = self
            backButton()
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
            view.addGestureRecognizer(tapGesture)
            
        
      
        
       
        
    }
    
    
    
    func setCategory(){
        categoryView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(categoryView)
        categoryView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        categoryView.leftAnchor.constraint(equalTo: view.leftAnchor,constant: 15).isActive = true
        categoryView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -15).isActive = true
        categoryView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        categoryView.backgroundColor = .secondarySystemBackground
        
        categoryTitle.translatesAutoresizingMaskIntoConstraints = false
        categoryView.addSubview(categoryTitle)
        categoryTitle.centerYAnchor.constraint(equalTo: categoryView.centerYAnchor).isActive = true
        categoryTitle.leftAnchor.constraint(equalTo: categoryView.leftAnchor,constant: 15).isActive = true
        categoryTitle.text = "Choose category"
       
        
        categoryIcon.translatesAutoresizingMaskIntoConstraints = false
        categoryView.addSubview(categoryIcon)
        categoryIcon.centerYAnchor.constraint(equalTo: categoryView.centerYAnchor).isActive = true
        categoryIcon.leftAnchor.constraint(equalTo: categoryView.leftAnchor,constant: 316).isActive = true
        categoryIcon.image = UIImage(named: "arrow")
        categoryIcon.heightAnchor.constraint(equalToConstant: 15).isActive = true
        categoryIcon.widthAnchor.constraint(equalToConstant: 8).isActive = true
        categoryView.layer.cornerRadius = 5
        
        // button
        categoryButton.translatesAutoresizingMaskIntoConstraints = false
        categoryView.addSubview(categoryButton)
        categoryButton.centerYAnchor.constraint(equalTo: categoryView.centerYAnchor).isActive = true
        categoryButton.centerXAnchor.constraint(equalTo: categoryView.centerXAnchor).isActive = true
        categoryButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        categoryButton.widthAnchor.constraint(equalToConstant: 335).isActive = true
        categoryButton.addTarget(self, action: #selector(catButtonTap), for: .touchUpInside)
    }
    
    private func setupTextField() {
        textField = CustomTextField(frame: .zero)
        view.addSubview(textField)
        //textField.delegate = self
        
        textField.topAnchor.constraint(equalTo: categoryView.bottomAnchor,constant: 11).isActive = true
        textField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        textField.heightAnchor.constraint(equalToConstant: 30).isActive = true
        textField.widthAnchor.constraint(equalToConstant: 335).isActive = true
 
    }
    
    func setupTextView() {
        textView = CustomTextView(frame: .zero)
        view.addSubview(textView)
        
        NSLayoutConstraint.activate([
           //textView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            textView.topAnchor.constraint(equalTo: textField.bottomAnchor, constant: 15),
            textView.leftAnchor.constraint(equalTo: view.leftAnchor,constant: 15),
            textView.rightAnchor.constraint(equalTo: view.rightAnchor,constant: -15),
            textView.heightAnchor.constraint(equalToConstant: 600),
         //   textView.widthAnchor.constraint(equalTo: view.widthAnchor)
            
        ])
        textView.backgroundColor = .secondarySystemBackground
        
        
        
        textField.text = item?.title
        textView.text = item?.body
        categoryTitle.text =  item?.category?.name
        categoryData = item?.category
    }
    // category button
    @objc func catButtonTap () {
        print("category button tapped")
        
        let bottomSheet = BottomSheetCV()
        bottomSheet.categoryData = categoryData
        bottomSheet.modalPresentationStyle = .formSheet
        present(bottomSheet, animated: true, completion: nil)
        if let sheet = bottomSheet.sheetPresentationController {
            sheet.detents = [
                .custom { _ in
                    return 400
                }
            ]
        }
        bottomSheet.didSelect = { [weak self] selected in
            DispatchQueue.main.async {
                self?.categoryTitle.text = selected.name
                self?.categoryData = selected
            }
           
        }
    }
    
    
    
    
    
    
     // back
    func backButton(){
        let backButton = UIButton(type: .system)
        backButton.setImage(UIImage(named: "back"), for: .normal)
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        // right button
        
        if item?.id != nil {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(updateTodo))
        } else {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(newTodo))
        }
        
    }
                            
                                         
                                         
    @objc func backButtonTapped(){
        navigationController?.popViewController(animated: true)
    }
    
    @objc  func newTodo() {
            guard let title = textField.text, let body = textView.text else {return}
        
            guard let userId = Auth.auth().currentUser?.uid else { return }
            let userRef = self.ref.child("users").child(userId)

            let todoReference = userRef.child("todo").childByAutoId()
            guard let autoId = todoReference.key else { return }
                           
            let newItem = ToDo(title: title, body: body, category: categoryData!, status: "undone", id: autoId)
           
            let newItemDic: [String: Any] = [
                "title": newItem.title!,
                "body": newItem.body!,
                "status": newItem.status!,
                "categoryId": categoryData?.id ?? "ww2",
                "id": newItem.id!
            ]
                todoReference.setValue(newItemDic)
                      
                navigationController?.popViewController(animated: true)
//        }
        
        
        
        
        
     
        
    }
    
    @objc func updateTodo(){
        
       
        
        let title = textField.text
                let body = textView.text
                let status = item?.status
                let categoryId = categoryData?.id
                let todoId = item?.id
                
                let updated: [String: Any] = [
                    "title": title!,
                    "body": body!,
                    "status": status!,
                    "categoryId": categoryId!,
                    "id": todoId!
                ]
        guard let userId = Auth.auth().currentUser?.uid else { return }
        let userRef = self.ref.child("users").child(userId)
        let categoryReference = userRef.child("todo").child(categoryId!)
        
        categoryReference.setValue(updated)
        let bottomSheet = BottomSheetCV()
        bottomSheet.categoryData = categoryData
        navigationController?.popViewController(animated: true)

    }
    
//    func textViewDidBeginEditing(_ textView: UITextView) {
//        if textView.textColor == UIColor.lightGray {
//            textView.text = nil
//            textView.textColor = UIColor.black
//        } else {
//            textView.textColor = UIColor.black
//        }
//
//    }
    
    
//    func textViewDidChange(_ textView: UITextView) {
////        if textView.textColor == UIColor.lightGray {
////                    textView.text = nil
////                    textView.textColor = UIColor.black
////                } else {
////                    textView.textColor = UIColor.black
////                }
//
//
//    }
//    func textFieldDidChangeSelection(_ textField: UITextField) {
//            // Update the mainTitle property of the item
//            
//        }
    
    
    
    
    
    
    
    @objc func hideKeyboard(){
        view.endEditing(true)
    }
    
    
    
}





