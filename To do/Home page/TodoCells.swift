//
//  TodoCells.swift
//  To do
//
//  Created by Ahmadxon Qodirov on 23/07/23.
//

import UIKit

class TodoCell: UITableViewCell {
    

    let back = UIView()
    let categoryColor = UIView()
    let doneIcon = UIImageView()
    let dateIcon = UIImageView()
    let dateLabel = UILabel()
    var mainTitle = UILabel()
    let subTitle = UILabel()
    
    
    
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = UIColor(red: 0.97, green: 0.97, blue: 0.97, alpha: 1)
        setView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setView() {
        back.translatesAutoresizingMaskIntoConstraints = false
        categoryColor.translatesAutoresizingMaskIntoConstraints = false
        doneIcon.translatesAutoresizingMaskIntoConstraints = false
        dateIcon.translatesAutoresizingMaskIntoConstraints = false
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        mainTitle.translatesAutoresizingMaskIntoConstraints = false
        subTitle.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(back)
        back.addSubview(categoryColor)
        back.addSubview(doneIcon)
        back.addSubview(dateIcon)
        back.addSubview(dateLabel)
        back.addSubview(mainTitle)
        back.addSubview(subTitle)
        
        NSLayoutConstraint.activate([
            back.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            back.leftAnchor.constraint(equalTo: leftAnchor,constant: 16),
            back.rightAnchor.constraint(equalTo: rightAnchor, constant: -16),
            back.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5),
            back.heightAnchor.constraint(equalToConstant: 71),
            

            categoryColor.topAnchor.constraint(equalTo: back.topAnchor),
            categoryColor.leftAnchor.constraint(equalTo: back.leftAnchor),
            categoryColor.heightAnchor.constraint(equalToConstant: 71),
            categoryColor.widthAnchor.constraint(equalToConstant: 4),
            
            
            doneIcon.centerYAnchor.constraint(equalTo: back.centerYAnchor),
            doneIcon.leftAnchor.constraint(equalTo: categoryColor.rightAnchor, constant: 10),
            doneIcon.heightAnchor.constraint(equalToConstant: 22),
            doneIcon.widthAnchor.constraint(equalToConstant: 22),
            
            
                  dateIcon.topAnchor.constraint(equalTo: back.topAnchor, constant: 7),
            dateIcon.leftAnchor.constraint(equalTo: categoryColor.rightAnchor, constant: 49),
            dateIcon.heightAnchor.constraint(equalToConstant: 12),
            dateIcon.widthAnchor.constraint(equalToConstant: 13),
            
            
            
            dateLabel.topAnchor.constraint(equalTo: back.topAnchor, constant: 6),
            dateLabel.leftAnchor.constraint(equalTo: dateIcon.rightAnchor,constant: 5),
            
            
            
            mainTitle.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: 5),
            mainTitle.leftAnchor.constraint(equalTo: doneIcon.rightAnchor, constant: 17),
            mainTitle.rightAnchor.constraint(equalTo: back.rightAnchor, constant: 10),
         
            
            
            
            subTitle.topAnchor.constraint(equalTo: mainTitle.bottomAnchor,constant: 3),
            subTitle.leftAnchor.constraint(equalTo: categoryColor.rightAnchor,constant: 51),
           // subTitle.bottomAnchor.constraint(equalTo: back.bottomAnchor,constant: -8)
           
            
        ])
        
        categoryColor.backgroundColor = .blue
        mainTitle.numberOfLines = 1
        mainTitle.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        subTitle.numberOfLines = 1
        subTitle.font = UIFont.systemFont(ofSize: 11, weight: .regular)
        subTitle.textColor = .lightGray
        back.layer.cornerRadius = 5
        back.backgroundColor = .white
       
        
        back.clipsToBounds = true
       // categoryColor.layer.cornerRadius = 5
      //  categoryColor.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        dateIcon.tintColor = .lightGray
        dateLabel.textColor = .lightGray
        dateLabel.font = UIFont.systemFont(ofSize: 11)
        
      
       
        
        
    }
    
    
    
    var item: ToDo? {
        didSet {
            
            dateIcon.image = UIImage(named: "date")
            doneIcon.image = UIImage(named: "undone")
            dateLabel.text = "20.02.2023"
           // dateLabel.text = item?.date
            mainTitle.text = item?.title
            subTitle.text = item?.body
           
            self.categoryColor.backgroundColor = UIColor(hex: self.item?.category?.color?.name ?? "FFFFFF" )
          
        }
    }
    
    
    
}
