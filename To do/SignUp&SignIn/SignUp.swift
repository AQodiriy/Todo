//
//  SignUp.swift
//  To do
//
//  Created by Ahmadxon Qodirov on 14/09/23.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class SignUp: BaseViewController, UITextFieldDelegate {
    
    
    let scrollView = UIScrollView()
    let logo = UIImageView()
    let email = UITextField()
    let password = UITextField()
    let repeatPassword = UITextField()
    let button = UIButton()
    let eyeIcon1 = UIButton()
    let eyeIcon2 = UIButton()
    
    let alert = UILabel()
    let databaseRef = Database.database().reference()
    
   
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setView()
        setupKeyboardHiding()
        password.isSecureTextEntry = true
        repeatPassword.isSecureTextEntry = true
        //registerUser()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        view.addGestureRecognizer(tapGesture)
        
        
      
    }
    
    

    
    func setView(){
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollView)
        scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        
        
        logo.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(logo)
        logo.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 140).isActive = true
        logo.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        logo.heightAnchor.constraint(equalToConstant: 62).isActive = true
        logo.widthAnchor.constraint(equalToConstant: 220).isActive = true
        logo.image = UIImage(named: "doit")
        
        
        email.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(email)
        email.topAnchor.constraint(equalTo: logo.bottomAnchor,constant: 65).isActive = true
        email.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        email.widthAnchor.constraint(equalToConstant: 343).isActive = true
        email.heightAnchor.constraint(equalToConstant: 52).isActive = true
        email.layer.cornerRadius = 12
        email.layer.borderWidth = 1
        email.layer.borderColor = UIColor.lightGray.cgColor
        email.placeholder = "Emailingizni kiriting"
        email.setLeftPaddingPoints(8)
        email.delegate = self
        
        password.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(password)
        password.topAnchor.constraint(equalTo: email.bottomAnchor,constant: 35).isActive = true
        password.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        password.widthAnchor.constraint(equalToConstant: 343).isActive = true
        password.heightAnchor.constraint(equalToConstant: 52).isActive = true
        password.layer.cornerRadius = 12
        password.layer.borderWidth = 1
        password.layer.borderColor = UIColor.lightGray.cgColor
        password.placeholder = "Parolingizni kiriting"
        password.setLeftPaddingPoints(8)
        password.delegate = self
        
        eyeIcon1.translatesAutoresizingMaskIntoConstraints = false
        password.addSubview(eyeIcon1)
        eyeIcon1.centerYAnchor.constraint(equalTo: password.centerYAnchor).isActive = true
        eyeIcon1.rightAnchor.constraint(equalTo: password.rightAnchor,constant: -16).isActive = true
        eyeIcon1.setImage(UIImage(named: "eyeOpen"), for: .normal)
        eyeIcon1.addTarget(self, action: #selector(secureOne), for: .touchUpInside)
        
        
        
        repeatPassword.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(repeatPassword)
        repeatPassword.topAnchor.constraint(equalTo: password.bottomAnchor,constant: 35).isActive = true
        repeatPassword.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        repeatPassword.widthAnchor.constraint(equalToConstant: 343).isActive = true
        repeatPassword.heightAnchor.constraint(equalToConstant: 52).isActive = true
        repeatPassword.layer.cornerRadius = 12
        repeatPassword.layer.borderWidth = 1
        repeatPassword.layer.borderColor = UIColor.lightGray.cgColor
        repeatPassword.placeholder = "Parolingizni takroran kiriting"
        repeatPassword.setLeftPaddingPoints(8)
        repeatPassword.delegate = self
        
        alert.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(alert)
        alert.topAnchor.constraint(equalTo: repeatPassword.bottomAnchor, constant: 10).isActive = true
        alert.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        alert.text = "Password do not match"
        alert.textColor = .systemRed
        alert.isHidden = true
        
        eyeIcon2.translatesAutoresizingMaskIntoConstraints = false
        repeatPassword.addSubview(eyeIcon2)
        eyeIcon2.centerYAnchor.constraint(equalTo: repeatPassword.centerYAnchor).isActive = true
        eyeIcon2.rightAnchor.constraint(equalTo: repeatPassword.rightAnchor,constant: -16).isActive = true
        eyeIcon2.setImage(UIImage(named: "eyeOpen"), for: .normal)
        eyeIcon2.addTarget(self, action: #selector(secureTwo), for: .touchUpInside)
        
        
        
        button.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(button)
        button.topAnchor.constraint(equalTo: repeatPassword.bottomAnchor,constant: 35).isActive = true
        button.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        button.widthAnchor.constraint(equalToConstant: 343).isActive = true
        button.heightAnchor.constraint(equalToConstant: 52).isActive = true
        button.layer.cornerRadius = 12
        button.addTarget(self, action: #selector(buttonTap), for: .touchUpInside)
        
        button.setTitle("Ro'yxatdan o'tish", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .systemBlue
        //  button.layer.borderWidth = 1
    }
    
    
    // auth
    func  registerUser(withEmail email: String, password: String){
        Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
            if let error = error {
                print("Error creating user: \(error.localizedDescription)")
            } else {
                
                if let user = authResult?.user {
                    print("User created: \(user.uid)")
                    
                    let userData =
                    ["email": email,
                     "password": password
                    ]
                   
                    self.databaseRef.child("users").child(user.uid).setValue(userData){ (error, ref) in
                        if let error = error {
                            print("Error saving user data: \(error.localizedDescription)")
                        } else {
                            print("User data saved successfully!")
                        }
                    }
                    
                }
            }
            
           
        }
        
     
    }
    
    @objc func buttonTap(){
        
        
        let email = email.text ?? ""
        let password = password.text ?? ""
        let repeatPassword = repeatPassword.text ?? ""
        
        
        if password == repeatPassword {
         
            print("SUCCESS with compatibility")
            registerUser(withEmail: email, password: password)
            alert.text = "Success"
            alert.textColor = .systemGreen
            
            
            
            navigationController?.pushViewController(TabBarController(), animated: true)
        } else {
            alert.isHidden = false
        }
        
       
    }
    
    
    
    
    func setupKeyboardHiding(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    @objc func secureOne(){
       
        password.isSecureTextEntry = !password.isSecureTextEntry
        eyeIcon1.setImage(UIImage(named: password.isSecureTextEntry ? "eyeOpen" : "eyeClose"), for: .normal)
        }
    
    @objc func secureTwo(){
       
        repeatPassword.isSecureTextEntry = !repeatPassword.isSecureTextEntry
        eyeIcon2.setImage(UIImage(named: repeatPassword.isSecureTextEntry ? "eyeOpen" : "eyeClose"), for: .normal)
    }
    
       
    @objc func handleTap(_ gesture: UITapGestureRecognizer) {
       
        email.resignFirstResponder()
        password.resignFirstResponder()
        repeatPassword.resignFirstResponder()
    }
        
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == email {
            password.becomeFirstResponder()
        } else if textField == password {
            repeatPassword.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    
    @objc func keyboardWillShow(sender: NSNotification) {
        guard let userInfo = sender.userInfo,
        let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
        let currentTextField = UIResponder.currentFirst() as? UITextField else {return}
        
        let keyboardTopY = keyboardFrame.cgRectValue.origin.y
        let convertedTextFieldFrame = view.convert(currentTextField.frame, from: currentTextField.superview)
        let textFieldBottomY = convertedTextFieldFrame.origin.y + convertedTextFieldFrame.size.height
       // let textFieldFrame = view.convert(currentTextField.frame, from: currentTextField.superview)
        
        if textFieldBottomY > keyboardTopY {
            let textBoxY = convertedTextFieldFrame.origin.y
            let newFrameY = (textBoxY - keyboardTopY / 2) * -1
            view.frame.origin.y = newFrameY
        }
    }
    @objc func keyboardWillHide(sender: NSNotification) {
        view.frame.origin.y = 0
    }
    
    
    
   

    
}






    
    
    
    
    
    
    
    

