//
//  SearchView.swift
//  To do
//
//  Created by Ahmadxon Qodirov on 23/07/23.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class HeaderView: UIView {
    
    
    
    var categoryList: [CategoryData] = []
    var itemList: [ToDo] = []
    var item: ToDo?
    let ref = Database.database().reference()
    
    let categoryController = CategoryController()
    let headerCell =  CategoryHeaderCell()
    let logo = UIImageView()
    let searchBar = UISearchBar()
    var selectedCategory: CategoryData?
    var previousSelectedCell: CategoryHeaderCell?
    
    var selectCategory:  ((CategoryHeaderCell) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        setView()
        fireBase()
        
        
       
        
    }
        required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        
        return collectionView
    }()
    
    func setView() {
        logo.translatesAutoresizingMaskIntoConstraints = false
        addSubview(logo)
        logo.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        logo.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        logo.widthAnchor.constraint(equalToConstant: 97).isActive = true
        logo.heightAnchor.constraint(equalToConstant: 28).isActive = true
        logo.image = UIImage(named: "doit")
        
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        addSubview(searchBar)
        searchBar.topAnchor.constraint(equalTo: logo.bottomAnchor, constant: 10).isActive = true
        searchBar.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10).isActive = true
       // searchBar.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10).isActive = true
        searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        searchBar.widthAnchor.constraint(equalToConstant: 347).isActive = true
        
        searchBar.backgroundImage = UIImage()
        searchBar.placeholder = "Search"
        searchBar.searchBarStyle = .minimal
        //searchBar.backgroundColor = .lightGray
        
        searchBar.delegate = self
        searchBar.showsCancelButton = false
        searchBar.searchBarStyle = .default
        searchBar.sizeToFit()
        
       
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
            addSubview(collectionView)
            collectionView.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 5).isActive = true
            collectionView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
            collectionView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -5).isActive = true
        // collectionView.heightAnchor.constraint(equalToConstant: 38).isActive = true
            collectionView.backgroundColor = UIColor(red: 0.97, green: 0.97, blue: 0.97, alpha: 1)
        
        
        collectionView.register(CategoryHeaderCell.self, forCellWithReuseIdentifier: CategoryHeaderCell.categoryHeaderIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsHorizontalScrollIndicator = false
    }
    func fireBase(){
        
        guard let userId = Auth.auth().currentUser?.uid else { return }
        let userRef = self.ref.child("users").child(userId)
        
        userRef.child("category").observe(.childAdded, with: { snapshot in
                   if let category = snapshot.value as? [String: Any] {
                   let name = category["name"] as? String
                   let color = category["color"] as? String
                   let id = category["id"] as? String
                   
                   let colorModel = ColorModel(color: color ?? "000000")
                   let categoryData = CategoryData(name: name ?? "no name", color: colorModel, id: id ?? "nil")
                       
                   self.categoryList.append(categoryData)
                    DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
           }
       })
        //updated
        userRef.child("category").observe(.childChanged, with: { [weak self] (snapshot) in
            if let data = snapshot.value as? [String: Any],
               let name = data["name"] as? String,
               let id = data["id"] as? String {

                let colorModel = ColorModel(color: "")
                let categoryData = CategoryData(name: name, color: colorModel, id: id)

                if let index = self?.categoryList.firstIndex(where: { $0.id == categoryData.id }) {
                    self?.categoryList[index] = categoryData
                    DispatchQueue.main.async {
                        self?.collectionView.reloadData()
                    }
                }
            }
        })

        // deleted
        userRef.child("category").observe(.childRemoved, with: { [weak self] (snapshot) in
            if let data = snapshot.value as? [String: Any],
               let id = data["id"] as? String {
                if let index = self?.categoryList.firstIndex(where: { $0.id == id }) {
                    self?.categoryList.remove(at: index)
                    DispatchQueue.main.async {
                        self?.collectionView.reloadData()
                    }
                }
            }
        })

    }
    
    
    
    func viewDidAppear(){
//        let indexPath = IndexPath(row: 0, section: 0)
//        collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .left)
        
        if categoryList.count > 0 {
                let indexPath = IndexPath(row: 0, section: 0)
                collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .left)
            }
    }
    
   
}


extension HeaderView: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    //    selectedCategory = categoryList[indexPath.row]
       
        previousSelectedCell?.backgroundColor = .white
       // previousSelectedCell?.layer.borderColor = UIColor.systemBlue.cgColor
    
        previousSelectedCell?.label.textColor = .black
        
        let selectedPath = IndexPath(row: indexPath.row, section: 0)
         let selected = collectionView.cellForItem(at: selectedPath) as? CategoryHeaderCell
        selected?.backgroundColor = UIColor.systemBlue
        
        selected?.label.textColor = .white
        self.previousSelectedCell = selected
        
        self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
      
        selectCategory!(selected!)
        
      
        
        }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryHeaderCell.categoryHeaderIdentifier, for: indexPath) as! CategoryHeaderCell
        
        
       // let filteredItems = categoryList.sorted(by: { $0.id == item?.category?.id })
       
        cell.category = categoryList[indexPath.row]
      //  cell.category? = filteredItems[indexPath.row]
        
        cell.label.textColor = .black
        cell.backgroundColor = .white
        cell.layer.cornerRadius = 7
        return cell
    }
    
    
}

// search
extension HeaderView: UISearchBarDelegate {
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.setShowsCancelButton(true, animated: true)
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
    }
}



//extension HeaderView: CategoryControllerDelegate {
//    func categoryController(_ controller: CategoryController, didAddNewItem item: CategoryData) {
//        categoryController.catList.append(item)
//        collectionView.reloadData()
//        
//    }
//    
//}




