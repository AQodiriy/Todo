//
//  CatBottomSheet.swift
//  To do
//
//  Created by Ahmadxon Qodirov on 08/08/23.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

protocol CatUpdate: AnyObject {
    func catUpdate(_ controller: CategoryBottomSheet, didModifyItem: CategoryData)
}


class CategoryBottomSheet: UIViewController,UITextFieldDelegate {
    
    let ref = Database.database().reference()
    weak var delegate: CatUpdate?
    
    let icon = UIImageView()
    let label = UILabel()
    let colorLabel = UILabel()
    var textField = UITextField()
    let button = UIButton()
    let color =  ColorPick()
    
    var catData: CategoryData?
    
    init(catData: CategoryData) {
        super.init(nibName: nil, bundle: nil)
        self.catData = catData
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setVew()
        setupTextField()
      //  let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
       // view.addGestureRecognizer(tapGesture)
        
        setupColor()
        setupButton()
        
        if let catItem = catData {
            textField.text = catItem.name
            color.selectedColor2 = UIColor(hex: (catItem.color?.name)!)!
            }
    }
        
    func setVew(){
        icon.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(icon)
        icon.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        icon.topAnchor.constraint(equalTo: view.topAnchor,constant: 82).isActive = true
        icon.image = UIImage(named: "setting")
        
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        label.topAnchor.constraint(equalTo: icon.bottomAnchor,constant: 20).isActive = true
        label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        label.text = "Add category"
        label.font = UIFont.systemFont(ofSize: 24, weight: .semibold)
        label.textColor = UIColor.black
        
        colorLabel.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(colorLabel)
        colorLabel.topAnchor.constraint(equalTo: label.bottomAnchor,constant: 146).isActive = true
        colorLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        colorLabel.text = "Choose color"
        colorLabel.font = UIFont.systemFont(ofSize: 24, weight: .semibold)
        colorLabel.textColor = UIColor.black
         
    }
    func setupColor() {
       
        color.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(color)
        color.topAnchor.constraint(equalTo: colorLabel.bottomAnchor,constant: 15).isActive = true
        color.leftAnchor.constraint(equalTo: view.leftAnchor,constant: 39).isActive = true
        color.rightAnchor.constraint(equalTo: view.rightAnchor,constant: -39).isActive = true
        
        color.heightAnchor.constraint(equalToConstant: 131).isActive = true
        
        color.selectedColors = { [weak self] color in
                self?.colorLabel.textColor = color
        }
        
            
        
    }
    func setupButton() {
        view.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.topAnchor.constraint(equalTo: color.bottomAnchor,constant: 38).isActive = true
        button.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        button.heightAnchor.constraint(equalToConstant: 59).isActive = true
        button.widthAnchor.constraint(equalToConstant: 199).isActive =  true
        button.layer.cornerRadius = 10
        button.setTitle("Done", for: .normal)
        button.backgroundColor = .systemBlue
        
        if  catData?.id != nil {
            button.addTarget(self, action: #selector(updateCategory), for: .touchUpInside)
        } else {
            button.addTarget(self, action: #selector(saveNewCategory), for: .touchUpInside)
        }
    }
    
    func setupTextField(){
        
            textField = CustomTextField(frame: .zero)
            view.addSubview(textField)
            textField.delegate = self
            
            textField.topAnchor.constraint(equalTo: icon.bottomAnchor,constant: 95).isActive = true
            textField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            textField.heightAnchor.constraint(equalToConstant: 48).isActive = true
            textField.widthAnchor.constraint(equalToConstant: 299).isActive = true
            textField.layer.cornerRadius = 10
            textField.backgroundColor = .secondarySystemBackground
            textField.font = UIFont.systemFont(ofSize: 20, weight: .regular)
            
            let paddingView = UIView(frame: CGRectMake(0, 0, 15, self.textField.frame.height))
            textField.leftView = paddingView
            textField.leftViewMode = UITextField.ViewMode.always

            let attributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.gray]
            textField.attributedPlaceholder = NSAttributedString(string: "Category name", attributes:attributes)

            textField.text = catData?.name
            color.selectedColor2 = UIColor.black
    }
    
    func clearTextField() {
            textField.text = ""
    }
    
    // MARK: - Write data to firebase
    @objc func saveNewCategory(){
            let hexString = color.selectedColor2.toHex
        
            guard let userId = Auth.auth().currentUser?.uid else { return }
            let userRef = self.ref.child("users").child(userId)
        
            guard let text = textField.text else { return  }
            let categoryReference = userRef.child("category").childByAutoId()
            let autoId = categoryReference.key
            
            let newItem = CategoryData(name: text, color: ColorModel(color: hexString ?? "ffffff"), id: autoId ?? "")
            let newItemDic: [String: Any] = [
                
                "name": newItem.name!,
                "color": newItem.color!.name!,
                "id": newItem.id!
            ]
        categoryReference.setValue(newItemDic)
        dismiss(animated: true, completion: nil)
        clearTextField()
    }
    // update
    @objc func updateCategory(){
        let categoryId = catData?.id
        let categoryName = textField.text
        let selectedColorHex = color.selectedColor2.toHex
        
        let data:  [AnyHashable : Any] = [
            "name": categoryName!,
            "id": categoryId!,
            "color": selectedColorHex!
        ]
        
        guard let userId = Auth.auth().currentUser?.uid else { return }
        let userRef = self.ref.child("users").child(userId)
        let categoryReference = userRef.child("category").child(categoryId!)
        categoryReference.setValue(data)

        if let existingItem = catData {
                        
            existingItem.name = categoryName
            existingItem.color?.name = selectedColorHex
            existingItem.id = categoryId
            delegate?.catUpdate(self, didModifyItem: existingItem)
        }
      
        dismiss(animated: true, completion: nil)
        clearTextField()
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            self.view.endEditing(true)
            return false
        }
}
