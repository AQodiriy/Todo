//
//  ExitProfile.swift
//  To do
//
//  Created by Ahmadxon Qodirov on 25/09/23.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class ExitProfile: BaseViewController {
    
    let databaseRef = Database.database().reference()
    
    
    let line = UIView()
    let text = UILabel()
    let cancel = UIButton()
    let confirm = UIButton()
    let group = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setView()
    }
    
    
    func setView(){
        line.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(line)
        line.topAnchor.constraint(equalTo: view.topAnchor, constant: 20).isActive = true
        line.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        line.heightAnchor.constraint(equalToConstant: 3).isActive = true
        line.widthAnchor.constraint(equalToConstant: 56).isActive = true
        line.backgroundColor = .black
        
        text.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(text)
        text.topAnchor.constraint(equalTo: line.bottomAnchor, constant: 18).isActive = true
        text.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        text.font = UIFont.systemFont(ofSize: 24, weight: .semibold)
        text.text = "Profildan chiqmoqchimisiz?"
        
        group.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(group)
        group.topAnchor.constraint(equalTo: view.topAnchor, constant: 95).isActive = true
        group.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        group.heightAnchor.constraint(equalToConstant: 43).isActive = true
        group.widthAnchor.constraint(equalToConstant: 328).isActive = true
        
        
        cancel.translatesAutoresizingMaskIntoConstraints = false
        group.addSubview(cancel)
        cancel.topAnchor.constraint(equalTo: group.topAnchor).isActive = true
        cancel.leftAnchor.constraint(equalTo: group.leftAnchor).isActive = true
        cancel.heightAnchor.constraint(equalToConstant: 43).isActive = true
        cancel.widthAnchor.constraint(equalToConstant: 173).isActive = true
        cancel.setTitle("Bekor qilish", for: .normal)
        cancel.backgroundColor = .systemBlue
        cancel.layer.cornerRadius = 12
        cancel.addTarget(self, action: #selector(cancelTap), for: .touchUpInside)
        
        confirm.translatesAutoresizingMaskIntoConstraints = false
        group.addSubview(confirm)
        confirm.topAnchor.constraint(equalTo: group.topAnchor).isActive = true
        confirm.leftAnchor.constraint(equalTo: cancel.rightAnchor,constant: 24).isActive = true
        confirm.heightAnchor.constraint(equalToConstant: 43).isActive = true
        confirm.widthAnchor.constraint(equalToConstant: 131).isActive = true
        confirm.setTitle("Ha", for: .normal)
        confirm.backgroundColor = .systemRed
        confirm.layer.cornerRadius = 12
        confirm.addTarget(self, action: #selector(confirmTap), for: .touchUpInside)
    }
    
    @objc func cancelTap(){
       dismiss(animated: true)
    }
    
    @objc func confirmTap(){
        do {
            try Auth.auth().signOut()
            print("Sign out successful")
            defaults.removeObject(forKey: "savedImage")
            
            let unregister = Unregistered()
            unregister.profileImg.isHidden = false
            unregister.profileText.isHidden = false
            unregister.penView.isHidden = false
            unregister.register.isHidden = false
            unregister.regIcon.isHidden = false
            unregister.regText.isHidden = false
            unregister.signin.isHidden = false
            unregister.signinIcon.isHidden = false
            unregister.signinText.isHidden = false
            
            // registered
            unregister.profileImg2.isHidden = true
            unregister.email.isHidden = true
            unregister.penView.isHidden = true
            unregister.share.isHidden = true
            unregister.shareIcon.isHidden = true
            unregister.shareText.isHidden = true
            unregister.logout.isHidden = true
            unregister.logoutIcon.isHidden = true
            unregister.logoutText.isHidden = true
        } catch let signOutError as NSError {
            print("Error signing out: %@", signOutError)
        }
    }
    
}
