//
//  ColorModel.swift
//  To do
//
//  Created by Ahmadxon Qodirov on 22/08/23.
//

import UIKit

class ColorModel: Codable {
    var id: Int? = nil
    var name: String? = nil
    
    init(color: String) {
        self.name = color
    }
}
