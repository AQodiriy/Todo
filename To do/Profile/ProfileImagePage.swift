//
//  ProfileImagePage.swift
//  To do
//
//  Created by Ahmadxon Qodirov on 24/09/23.
//

import UIKit

class ProfileImage: BaseViewController{
    
    
    
    var selectedIndexPath: IndexPath?
    
    var selectedImage = ""
   
    
    
    let button = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setView()
        
        
    }
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
       // layout.itemSize = CGSize(width: 80, height: 80)
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "MyCell")
        return collectionView
    }()
    
    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            self.navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationController?.navigationBar.topItem?.title = "Rasm o'zgartirish"
        navigationController?.navigationBar.topItem?.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector( backButton))
        }
    

    
    
    
    
    func setView(){
        view.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 10).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor,constant: 36).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor,constant: -36).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor,constant: -130).isActive = true
        //collectionView.backgroundColor = .green
        collectionView.delegate = self
        collectionView.dataSource = self
        
        view.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.topAnchor.constraint(equalTo: collectionView.bottomAnchor, constant: 5).isActive = true
        button.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        button.heightAnchor.constraint(equalToConstant: 56).isActive = true
        button.widthAnchor.constraint(equalToConstant: 343).isActive = true
        button.backgroundColor = .systemBlue
        button.setTitle("Saqlash", for: .normal)
        button.layer.cornerRadius = 12
        
        button.addTarget(self, action: #selector(buttonTap), for: .touchUpInside)
        
       
       
    }
    
    
    @objc func backButton(){
       
            navigationController?.popViewController(animated: true)

       
    }
    
    @objc func buttonTap(){
       
        guard let selectedIndexPath = selectedIndexPath else {return}
            defaults.set(selectedImage, forKey: "savedImage")
            print("selectedImage -> : \(selectedImage)")
            navigationController?.popViewController(animated: true)
    }
}



extension ProfileImage: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource {
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell", for: indexPath)
        //cell.backgroundColor = .blue
        
        let image = imageData[indexPath.row]
        var config = UIListContentConfiguration.cell()
        config.image = UIImage(named: image)
        cell.contentConfiguration = config
        cell.layer.cornerRadius = 12
       
        if selectedIndexPath == indexPath {
                cell.isSelected = true
                cell.layer.borderWidth = 2.0
                cell.layer.borderColor = UIColor.orange.cgColor
            

            print("this is image -> \( image)")
                
            } else {
                cell.isSelected = false
                cell.layer.borderWidth = 0.0
                cell.layer.borderColor = nil
            }
            
        
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        selectedIndexPath = indexPath
        selectedImage = imageData[indexPath.row]
        defaults.set(selectedImage, forKey: "savedImage")
       
        
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 80)
    }
    
    
    
   
    
    
}
