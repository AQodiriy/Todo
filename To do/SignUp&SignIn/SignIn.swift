//
//  SignIn.swift
//  To do
//
//  Created by Ahmadxon Qodirov on 17/09/23.
//

import UIKit


class SignIn: BaseViewController, UITextFieldDelegate {
    
    
    let scrollView = UIScrollView()
    let logo = UIImageView()
    let email = UITextField()
    let password = UITextField()
    let button = UIButton()
    let eyeIcon = UIButton()
    
    let errorView = UIView()
    let errorText = UILabel()
    let errorImage = UIImageView()
    
    
   
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setView()
        setupKeyboardHiding()
        password.isSecureTextEntry = true
       
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        view.addGestureRecognizer(tapGesture)
        
        
    }
    

    
    func setView(){
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollView)
        scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        
        
        logo.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(logo)
        logo.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 140).isActive = true
        logo.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        logo.heightAnchor.constraint(equalToConstant: 62).isActive = true
        logo.widthAnchor.constraint(equalToConstant: 220).isActive = true
        logo.image = UIImage(named: "doit")
        
        
        email.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(email)
        email.topAnchor.constraint(equalTo: logo.bottomAnchor,constant: 65).isActive = true
        email.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        email.widthAnchor.constraint(equalToConstant: 343).isActive = true
        email.heightAnchor.constraint(equalToConstant: 52).isActive = true
        email.layer.cornerRadius = 12
        email.layer.borderWidth = 1
        email.layer.borderColor = UIColor.lightGray.cgColor
        email.placeholder = "Emailingizni kiriting"
        email.setLeftPaddingPoints(8)
        email.delegate = self
        
        password.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(password)
        password.topAnchor.constraint(equalTo: email.bottomAnchor,constant: 35).isActive = true
        password.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        password.widthAnchor.constraint(equalToConstant: 343).isActive = true
        password.heightAnchor.constraint(equalToConstant: 52).isActive = true
        password.layer.cornerRadius = 12
        password.layer.borderWidth = 1
        password.layer.borderColor = UIColor.lightGray.cgColor
        password.placeholder = "Parolingizni kiriting"
        password.setLeftPaddingPoints(8)
        password.delegate = self
        
        eyeIcon.translatesAutoresizingMaskIntoConstraints = false
        password.addSubview(eyeIcon)
        eyeIcon.centerYAnchor.constraint(equalTo: password.centerYAnchor).isActive = true
        eyeIcon.rightAnchor.constraint(equalTo: password.rightAnchor,constant: -16).isActive = true
        eyeIcon.setImage(UIImage(named: "eyeOpen"), for: .normal)
        eyeIcon.addTarget(self, action: #selector(secureOne), for: .touchUpInside)
        
        button.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(button)
        button.topAnchor.constraint(equalTo: password.bottomAnchor,constant: 35).isActive = true
        button.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        button.widthAnchor.constraint(equalToConstant: 343).isActive = true
        button.heightAnchor.constraint(equalToConstant: 52).isActive = true
        button.layer.cornerRadius = 12
        
        
        button.setTitle("Ro'yxatdan o'tish", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .systemBlue
        //  button.layer.borderWidth = 1
        
        
        errorView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(errorView)
        errorView.topAnchor.constraint(equalTo: button.bottomAnchor,constant: 130).isActive = true
        errorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        errorView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        errorView.widthAnchor.constraint(equalToConstant: 281).isActive = true
        errorView.backgroundColor = .red
        errorView.layer.cornerRadius = 15
        
        errorText.translatesAutoresizingMaskIntoConstraints = false
        errorView.addSubview(errorText)
        errorText.centerXAnchor.constraint(equalTo: errorView.centerXAnchor).isActive = true
        errorText.centerYAnchor.constraint(equalTo: errorView.centerYAnchor).isActive = true
        errorText.text = "Email yoki parolingiz xato"
        errorText.textColor = .white
        
        errorImage.translatesAutoresizingMaskIntoConstraints = false
        errorView.addSubview(errorImage)
        errorImage.centerYAnchor.constraint(equalTo: errorView.centerYAnchor).isActive = true
        errorImage.leftAnchor.constraint(equalTo: errorView.leftAnchor,constant: 24).isActive = true
        errorImage.image = UIImage(named: "error")
        
    }
    func setupKeyboardHiding(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    @objc func secureOne(){
       
        password.isSecureTextEntry = !password.isSecureTextEntry
        eyeIcon.setImage(UIImage(named: password.isSecureTextEntry ? "eyeOpen" : "eyeClose"), for: .normal)
        }
    
    
    
       
    @objc func handleTap(_ gesture: UITapGestureRecognizer) {
       
        email.resignFirstResponder()
        password.resignFirstResponder()

    }
        
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == email {
            password.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    
    @objc func keyboardWillShow(sender: NSNotification) {
        guard let userInfo = sender.userInfo,
        let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
        let currentTextField = UIResponder.currentFirst() as? UITextField else {return}
        
        let keyboardTopY = keyboardFrame.cgRectValue.origin.y
        let convertedTextFieldFrame = view.convert(currentTextField.frame, from: currentTextField.superview)
        let textFieldBottomY = convertedTextFieldFrame.origin.y + convertedTextFieldFrame.size.height
       // let textFieldFrame = view.convert(currentTextField.frame, from: currentTextField.superview)
        
        if textFieldBottomY > keyboardTopY {
            let textBoxY = convertedTextFieldFrame.origin.y
            let newFrameY = (textBoxY - keyboardTopY / 2) * -1
            view.frame.origin.y = newFrameY
        }
        
        
    }
    @objc func keyboardWillHide(sender: NSNotification) {
        view.frame.origin.y = 0
    }

    
}

