//
//  CategoryCell.swift
//  To do
//
//  Created by Ahmadxon Qodirov on 07/08/23.
//

import UIKit

class CategoryCell: UITableViewCell {
    
    
    let label = UILabel()
    var back = UIView()
    let colorView = UIView()
    let color = ColorPick()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setView()
        
        
        
        
        
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setView() {
        contentView.backgroundColor = UIColor(red: 0.97, green: 0.97, blue: 0.97, alpha: 1)
        
        
        back.translatesAutoresizingMaskIntoConstraints = false
        addSubview(back)
        back.topAnchor.constraint(equalTo: topAnchor,constant: 3.5).isActive = true
        back.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        back.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        back.heightAnchor.constraint(equalToConstant: 48).isActive = true
        back.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -1).isActive = true
        back.backgroundColor = .white
        back.layer.cornerRadius = 8
        back.clipsToBounds = true
        
        colorView.translatesAutoresizingMaskIntoConstraints = false
        back.addSubview(colorView)
        colorView.topAnchor.constraint(equalTo: back.topAnchor).isActive = true
        colorView.bottomAnchor.constraint(equalTo: back.bottomAnchor).isActive = true
        colorView.leftAnchor.constraint(equalTo: back.leftAnchor).isActive = true
        colorView.widthAnchor.constraint(equalToConstant: 4).isActive = true
        
        label.translatesAutoresizingMaskIntoConstraints = false
        back.addSubview(label)
        label.centerYAnchor.constraint(equalTo: back.centerYAnchor).isActive = true
        label.leftAnchor.constraint(equalTo: colorView.rightAnchor,constant: 5).isActive = true
        label.bottomAnchor.constraint(equalTo: back.bottomAnchor).isActive = true
        selectionStyle = .none
        
       
        
    }
    
    
    
    var category: CategoryData? {
        didSet {
            label.text = category?.name
          //  back.backgroundColor = category?.color
            colorView.backgroundColor = UIColor(hex: category?.color?.name ?? "FFFFFF" )
            
           
           
        }
        
        
    }
    
   
    
    
}
