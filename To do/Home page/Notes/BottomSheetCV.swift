
import UIKit
import FirebaseDatabase
import FirebaseAuth





class BottomSheetCV: UIViewController {
    
   
    var categoryList: [CategoryData] = []
    //var categoryData: CategoryData?
    var ref = Database.database().reference()
    
    let tableView = UITableView()
    var categoryData: CategoryData?
    var didSelect: ((CategoryData) -> Void)?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpTable()
        fireBase()
    }
    
    func setUpTable(){
        
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
       // tableView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
    }
    
    func fireBase(){

        guard let userId = Auth.auth().currentUser?.uid else { return }
        let userRef = self.ref.child("users").child(userId)
        
        userRef.child("category").observe( .childAdded, with: { snapshot in
                   
                   if let category = snapshot.value as? [String: Any] {
                   let name = category["name"] as? String
                   let color = category["color"] as? String
                   let id = category["id"] as? String
                       
                   // let colorId = color["id"] as? String
                   //  let colorName = color?["color"] as? String
                   //  print("Color name: -> \(colorName) ;; Color id: \(colorId)")
                   
                   let colorModel = ColorModel(color: color ?? "000000")
                   let categoryData = CategoryData(name: name ?? "no name", color: colorModel, id: id ?? "nil")
                       
                   self.categoryList.append(categoryData)
                    DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                      
           }
            
       })
        //updated
        userRef.child("category").observe(.childChanged, with: { [weak self] (snapshot) in
            if let data = snapshot.value as? [String: Any],
               let name = data["name"] as? String,
               let id = data["id"] as? String {

                let colorModel = ColorModel(color: "")
                let categoryData = CategoryData(name: name, color: colorModel, id: id)

                if let index = self?.categoryList.firstIndex(where: { $0.id == categoryData.id }) {
                    self?.categoryList[index] = categoryData
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                    }
                }
            }
        })

        // deleted
        userRef.child("category").observe(.childRemoved, with: { [weak self] (snapshot) in
            if let data = snapshot.value as? [String: Any],
               let id = data["id"] as? String {
                if let index = self?.categoryList.firstIndex(where: { $0.id == id }) {
                    self?.categoryList.remove(at: index)
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                    }
                }
            }
        })

    }
    
}

extension BottomSheetCV: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        
        let category = categoryList[indexPath.row]
        var content = cell.defaultContentConfiguration()
        content.text = category.name
        cell.contentConfiguration = content

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedRow = categoryList[indexPath.row]
       
        tableView.deselectRow(at: indexPath, animated: true)
        didSelect?(selectedRow)
        tableView.reloadData()
        self.dismiss(animated: true, completion: nil)
        

    }
    
    
    
    
}
