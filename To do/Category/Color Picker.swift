//
//  Color Picker.swift
//  To do
//
//  Created by Ahmadxon Qodirov on 08/08/23.
//

import UIKit






class ColorPick: UIView {
   
   
    var selectedIndexPath: IndexPath?
    
    var selectedColor2 = UIColor()
    var selectedColors: ((UIColor)-> Void) = { _ in }
    let colors: [UIColor] = [ UIColor(red: 0.9, green: 0.32, blue: 0.9,
                                      alpha: 1),
                              UIColor(red: 1, green: 0.86, blue: 0.35, alpha: 1),
                              UIColor(red: 0.34, green: 0.8, blue: 0.6, alpha: 1), UIColor(red: 0.22, green: 0.69, blue: 0.78, alpha: 1),
                              UIColor(red: 0.91, green: 0.24, blue: 0.25, alpha: 1),
                              UIColor(red: 0.9, green: 0.9, blue: 0.9,alpha: 1),
                              UIColor(red: 0.59, green: 0.46, blue: 0.97,alpha: 1),
                              UIColor(red: 1, green: 0.64, blue: 0,alpha: 1)]
//     var colorSelected: Bool {
//        selectedIndexPath != nil
//    }
    
    
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUp()
        
        
        
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 59, height: 59)
        
      
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        
        
        
        
        
        return collectionView
    }()
    
    private func setUp() {
        
       
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        
        
        
        addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
//        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalToConstant: 131).isActive = true
        collectionView.backgroundColor = .white
         

        
        
        
        }
    
    
      
}

extension ColorPick: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        
        
        
        cell.layer.cornerRadius = 30
       
        cell.backgroundColor = colors[indexPath.row]
        
        
        if selectedIndexPath == indexPath {
                cell.isSelected = true
            cell.contentView.layer.borderWidth = 6.0
                    cell.contentView.layer.cornerRadius = 30
                    cell.contentView.layer.borderColor = UIColor.white.cgColor
            
                    cell.layer.borderWidth = 2.0
                    cell.layer.borderColor = colors[indexPath.row].cgColor
                    selectedColors(colors[indexPath.row])
                    selectedColor2 = colors[indexPath.row]
            } else {
                cell.isSelected = false
                cell.contentView.layer.borderWidth = 0.0
                        cell.contentView.layer.cornerRadius = 0.0
                        cell.contentView.layer.borderColor = nil
                
                        cell.layer.borderWidth = 0.0
                        cell.layer.borderColor = nil
            }
        
        
        

        return cell
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     //   let cell = collectionView.cellForItem(at: indexPath)
        
        selectedIndexPath = indexPath
            collectionView.reloadData()
        
        
    }
    
    
}
