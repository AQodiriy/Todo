//
//  BaseViewController.swift
//  To do
//
//  Created by Ahmadxon Qodirov on 29/08/23.
//

import UIKit

class BaseViewController: UIViewController {
    
    
    let defaults = UserDefaults.standard
    let imageData = [
        "1", "2","3","4","5","6"
    ]
    
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
    }
}
