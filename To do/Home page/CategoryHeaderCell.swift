//
//  CategoryHeader.swift
//  To do
//
//  Created by Ahmadxon Qodirov on 25/07/23.
//

import UIKit

class CategoryHeaderCell: UICollectionViewCell {
    
    static let categoryHeaderIdentifier = "CategoryHeaderCell"
    var label = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setView() {
        label.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label)
        label.topAnchor.constraint(equalTo: topAnchor,constant: 10).isActive = true
        label.leftAnchor.constraint(equalTo: leftAnchor,constant: 10).isActive = true
        label.rightAnchor.constraint(equalTo: rightAnchor,constant: -10).isActive = true
        label.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -10).isActive = true
       
    }
    
    
    var category: CategoryData? {
        didSet {
            label.text = category?.name
            
        }
    }
    
}
